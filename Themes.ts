export  const  darkTheme = {
    mode: 'dark',
    PRIMARY_BG: '#212121',
    PRIMARY_TXT: '#ffffff',
    PRIMARY_SELECTED_TXT: '#f7ff00',
    PRIMARY_BTN: '#363535',
    PRIMARY_BTN_TXT: '#ffffff',
    STATUS_BAR: 'default'
}

export const lightTheme = {
    mode: 'light',
    PRIMARY_BG: '#ffffff',
    PRIMARY_TXT: '#212121',
    PRIMARY_SELECTED_TXT: '#ff0000',
    PRIMARY_BTN: '#8022d9',
    PRIMARY_BTN_TXT: '#ffffff',
    STATUS_BAR: 'light-content'
}
