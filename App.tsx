import {Provider} from 'react-redux';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk'
import themeReducer from "./src/redux/themeReducer";
import HomePage from "./src/pages/HomePage";
import './src/constants/IMLocalize';

const store = createStore(combineReducers({themeReducer}), applyMiddleware(thunk))
export default function App() {
  return (
    <Provider store={store}>
      <HomePage/>
    </Provider>
  );
}
