import React from "react";
import styled, {ThemeProvider} from "styled-components/native";
import {useSelector} from "react-redux";

export default function LangLabel(props: any) {
    const theme = useSelector((state: any) => state.themeReducer.theme)
    const selected = props.isSelected;

    const Label = styled.Text`
      font-size: 18px;
      padding: 4px 0;
      color: ${(props: any) => selected ? props.theme.PRIMARY_SELECTED_TXT : props.theme.PRIMARY_TXT};
    `

    return (
        <ThemeProvider theme={theme}>
            <Label>{props.label}</Label>
        </ThemeProvider>
    )

}


