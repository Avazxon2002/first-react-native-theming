import React from 'react';
import {useTranslation} from 'react-i18next';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styled, {ThemeProvider} from "styled-components/native";
import {useSelector} from "react-redux";
import LangLabel from "./LangLabel";

const LANGUAGES = [
    {code: 'en', label: 'English'},
    {code: 'ru', label: 'Russian'},
    {code: 'uz', label: 'Uzbek'}
];
const STORE_LANGUAGE_KEY = "settings.lang";

function Selector() {
    const theme = useSelector((state: any) => state.themeReducer.theme)
    const {t, i18n} = useTranslation()
    const selectedLang = i18n.language

    const setLang = (lang: string) => {
        console.log('selected: ' + lang)
        console.log('current: ' + i18n.language)
        i18n.changeLanguage(lang).finally(() => {
            AsyncStorage.setItem(STORE_LANGUAGE_KEY, lang);
            console.log('changed to: ' + i18n.language)
        })
    }

    return (
        <ThemeProvider theme={theme}>
            <Container>
                <Row>
                    <Title>{t('languageSelector')}</Title>
                </Row>
                {LANGUAGES.map(language => {
                    const selectedLanguage = language.code === selectedLang;

                    return (
                        <ButtonText key={language.code}>
                            <Clicker disabled={selectedLanguage}
                                     onPress={() => setLang(language.code)}>
                                <LangLabel isSelected={selectedLanguage}
                                           theme={theme}
                                           label={language.label}/>
                            </Clicker>
                        </ButtonText>
                    );
                })}
            </Container>
        </ThemeProvider>
    )
}

const Container = styled.View`
  padding: 60px 16px 0 16px;
`

const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const Title = styled.Text`
  font-size: 28px;
  font-weight: 600;
  color: ${(props: any) => props.theme.PRIMARY_TXT};
`

const ButtonText = styled.Text`
  font-size: 15px;
  font-weight: 500;
`

const Clicker = styled.Pressable`
  margin-top: 10px;
`

export default Selector
