import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {en, ru, uz} from "./translations";
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Localization from 'expo-localization';
import RNLanguageDetector from '@os-team/i18next-react-native-language-detector';

const LANGUAGES = {
    en: {
        translation: en,
    },
    ru: {
        translation: ru,
    },
    uz: {
        translation: uz,
    },
};

const STORE_LANGUAGE_KEY = "settings.lang";

i18n
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    .use(RNLanguageDetector)
    // set options
    .init({
        compatibilityJSON: 'v3',
        resources: LANGUAGES,
        interpolation: {
            escapeValue: false
        },
        fallbackLng: "en"
    }, async function (callback: (lang: string) => void) {
        try {
            await AsyncStorage.getItem(STORE_LANGUAGE_KEY).then((language) => {
                if (language) {
                    i18n.changeLanguage(language)
                } else {
                    i18n.changeLanguage(Localization.locale);
                }
            });
        } catch (error) {
            console.log("Error reading language", error);
        }
    });

export default i18n
