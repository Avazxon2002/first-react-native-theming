import {useDispatch, useSelector} from "react-redux";
import styled, {ThemeProvider} from "styled-components/native";
import {StatusBar, Text, View} from "react-native";
import {darkTheme, lightTheme} from "../../Themes";
import {switchTheme} from "../redux/themeAction";
import {useTranslation} from 'react-i18next';
import Selector from "../components/LanguageSelector";
import React from "react";

export default function HomePage() {
    const theme = useSelector((state: any) => state.themeReducer.theme)
    const dispatch = useDispatch()
    const {t} = useTranslation()

    return (
        <ThemeProvider theme={theme}>
            <StatusBar barStyle={theme.STATUS_BAR}/>
            <Container>
                <TextContainer>
                    <TextTheme>
                        <Title>{t('hello')}</Title>
                    </TextTheme>

                </TextContainer>

                <Selector/>

                {theme.mode == 'light' ? (
                    <Button onPress={() => dispatch(switchTheme(darkTheme))}>
                        <ButtonText>
                            {t('button')}
                        </ButtonText>
                    </Button>) : (
                    <Button onPress={() => dispatch(switchTheme(lightTheme))}>
                        <ButtonText>
                            {t('button')}
                        </ButtonText>
                    </Button>
                )}

            </Container>
        </ThemeProvider>
    )
}

const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: ${(props: any) => props.theme.PRIMARY_BG};
`

const TextContainer = styled.View`
  border: 1px solid ${(props: any) => props.theme.PRIMARY_TXT};
  border-radius: 6px;
  padding: 16px;
`

const Button = styled.TouchableOpacity`
  margin: 32px 0;
  background-color: ${(props: any) => props.theme.PRIMARY_BTN};
  padding: 16px 32px;
`

const ButtonText = styled.Text`
  font-size: 15px;
  font-weight: 500;
  color: ${(props: any) => props.theme.PRIMARY_BTN_TXT};
`

const TextTheme = styled.Text`
  font-size: 15px;
  font-weight: 500;
  color: ${(props: any) => props.theme.PRIMARY_TXT};
`

const Title = styled.Text`
  font-size: 15px;
  font-weight: 600;
  color: ${(props: any) => props.theme.PRIMARY_TXT};
`


